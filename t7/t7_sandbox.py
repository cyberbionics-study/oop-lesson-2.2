import t7


a = t7.Ship('HMS Dreadnought', "Royal Navy's battleship")
b = t7.Mount('Sleipnir', 'Asgardian horse')
c = t7.Aircraft('Huey', 'Bell UH-1 Iroquois, utility helicopter')

a.define()
a.travel()

b.define()
b.travel()

c.define()
c.travel()
