class Transport:
    def __init__(self, name):
        self.name = name

    def travel(self):
        print(f'{self.name} is relocating you\n')


class Engined:
    power = 'powered by engine\n'


class RidingTransport(Transport):
    def __init__(self, name, spec):
        self.name = name
        self.spec = spec

    def define(self):
        print(f'{self.name} is {self.spec}\n')


class FlyingTransport(Transport):
    def __init__(self, name, spec):
        self.name = name
        self.spec = spec

    def define(self):
        print(f'{self.name} is {self.spec}\n')


class SailingTransport(Transport):
    def __init__(self, name, spec):
        self.name = name
        self.spec = spec

    def define(self):
        print(f'{self.name} is {self.spec}\n')


class Vehicle(RidingTransport, Engined):
    def travel(self):
        print(f'you drive {self.name} to some place\n')


class Mount(RidingTransport):
    def travel(self):
        print(f'{self.name} carries you to your spot\n')


class Aircraft(FlyingTransport, Engined):
    def travel(self):
        print(f'you fly by {self.name} to your target\n')


class Sailer(SailingTransport):
    def travel(self):
        print(f"{self.name}'s canvases cruise you by the waves\n")


class Boat(SailingTransport):
    def travel(self):
        print(f'the noise of the oars of {self.name} accompanies you in your journey\n')


class Ship(SailingTransport, Engined):
    def travel(self):
        print(f"smoke of {self.name}'s pypes marks your path\n")


class Amphibian(RidingTransport, SailingTransport, Engined):
    def travel(self):
        print(f'{self.name} drives you by the surface\n')


class Teleport(Transport):
    pass
