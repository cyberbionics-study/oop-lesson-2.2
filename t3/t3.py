class BaseClass1:
    attribute = 'base attribute'

    def some_func(self):
        print(f'this is function of base class 1')


class BaseClass2:
    apanage = 'base apanage'

    def some_feat(self):
        print(f'this is feature of base class 2')


class SomeClass1(BaseClass1):
    attribute = 'modified attribute '

    def some_func(self):
        print('this is function of modified class 1')


class SomeClass2(BaseClass2):
    apanage = 'modified apanage'

    def some_feat(self):
        print('this is feature of modified class 2')


class MultiClass(BaseClass1, SomeClass2):
    pass


x = MultiClass()
x.some_func()
x.some_feat()
print(x.attribute)
print(x.apanage)
