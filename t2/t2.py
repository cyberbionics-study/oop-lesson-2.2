class GraphicObject:
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def draw(self):
        pass

    def process_click(self):
        pass


class Rectangle(GraphicObject):
    def __init__(self, x, y, width, height, color):
        super().__init__(x, y, width, height)
        self.color = color

    def draw(self):
        pass


class Button(Rectangle):
    def __init__(self, x, y, width, height, text):
        super().__init__(x, y, width, height, color='blue')
        self.text = text

    def draw(self):
        pass

    def process_click(self):
        print(f"Кнопка с текстом '{self.text}' была нажата.")

b = Button(50, 50, 200, 60, 'sometext')
b.process_click()