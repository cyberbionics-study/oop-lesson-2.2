from datetime import date


class MyClass1:
    people = []

    def __init__(self, surname, name, age):
        self.surname = surname
        self.name = name
        self.age = age
        MyClass1.people.append(self)

    def print_info(self):
        print(self.surname + " " + self.name + "'s age is: " + str(self.age))

    @staticmethod
    def is_adult_ukraine(age):
        return age >= 18

    @staticmethod
    def is_adult_usa(age):
        return age >= 21

    @classmethod
    def from_birth_year(cls, surname, name, birth_year):
        return cls(surname, name, date.today().year - birth_year)

    @classmethod
    def count_adults(cls, country):
        count = 0
        for person in cls.people:
            if country == "Ukraine" and cls.is_adult_ukraine(person.age):
                count += 1
            elif country == "USA" and cls.is_adult_usa(person.age):
                count += 1
        return count


class MyClass2(MyClass1):
    color = 'White'


m_per1 = MyClass1('Ivanenko', 'Ivan', 19)
m_per1.print_info()

m_per2 = MyClass1.from_birth_year('Dovzhenko', 'Bogdan',  2000)
m_per2.print_info()

m_per3 = MyClass2.from_birth_year('Sydorchuk', 'Petro', 2010)
print(isinstance(m_per3, MyClass2))

m_per4 = MyClass2.from_birth_year('Makuschenko', 'Dmytro', 2001)
print(isinstance(m_per4, MyClass1))

print(issubclass(MyClass1, MyClass2))
print(issubclass(MyClass2, MyClass1))
