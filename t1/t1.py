class Editor:
    def __init__(self, file):
        self.file = file

    def view_document(self):
        with open(self.file, 'r') as f:
            print(f.read())

    def edit_document(self):
        print('Editing is unavailable in free version')


class ProEditor(Editor):
    valid_keys = [12345, 12346, 12347, 12348, 12349, 12340]

    def __init__(self, file):
        key = int(input('your key : '))
        if key in self.valid_keys:
            super().__init__(file)
            self.key = key
            print('key is valid')
        else:
            print('Invalid key. Access failed')

    def edit_document(self):
        with open(self.file, 'a') as f:
            new_content = f'\n' + input('Enter the new content: ')
            f.write(new_content)
